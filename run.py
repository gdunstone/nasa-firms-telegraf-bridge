#!/bin/python
import schedule
import time
import requests
import io
import traceback
import datetime
import csv
from telegraf.client import TelegrafClient
# from telegraf.protocol import Line

client = TelegrafClient(host='telegraf-public', port=8092)

MODIS_URL = "https://firms.modaps.eosdis.nasa.gov/data/active_fire/c6/csv/MODIS_C6_Australia_NewZealand_24h.csv"

VIIRS_SNPP_URL = "https://firms.modaps.eosdis.nasa.gov/data/active_fire/viirs/csv/VNP14IMGTDL_NRT_Australia_NewZealand_24h.csv"

VIIRS_NOAA_URL = "https://firms.modaps.eosdis.nasa.gov/data/active_fire/noaa-20-viirs-c2/csv/J1_VIIRS_C2_Australia_NewZealand_24h.csv"

urls = {"modis": MODIS_URL, "viirs_noaa": VIIRS_NOAA_URL, "viirs_snpp": VIIRS_SNPP_URL}


def collect():
    for n, url in urls.items():
        try:
            response = requests.get(MODIS_URL)
            if not response.ok:
                print(response.reason)
                continue
            i = io.StringIO(response.content.decode())
            i.seek(0)

            reader = csv.DictReader(i)

            for q, row in enumerate(reader):
                try:
                    # 'acq_date': '2020-02-04', 'acq_time': '1806',
                    t = datetime.datetime.strptime(f"{row.pop('acq_date')} {row.pop('acq_time')}.{q}", "%Y-%m-%d %H%M.%f")
                    tags = dict()
                    fields = dict()
                    for k, v in row.items():
                        try:
                            fields[k] = float(v)
                        except:
                            tags[k] = v
                    tags["collected_from"] = n
                    # line = Line(n, fields, tags, int(t.timestamp() * 1000000000))
                    time.sleep(0.001)  # wait a little bit to not overload telegraf...
                    client.metric("firms.modaps.eosdis.nasa.gov", fields, tags=tags, timestamp=int(t.timestamp() * 1000000000))
                except Exception:
                    traceback.print_exc()
        except Exception:
            traceback.print_exc()
        time.sleep(60 * 60)


schedule.every(6).hours.do(collect)
if __name__ == "__main__":
    collect()
    while True:
        schedule.run_pending()
        time.sleep(1)
